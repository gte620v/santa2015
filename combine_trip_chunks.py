import time, glob
import shutil
import sys

folder = sys.argv[1]


outfilename = folder + ".csv"


with open(outfilename, 'wb') as outfile:
    for filename in glob.glob(folder+ '/*'):
        if filename == outfilename:
            # don't want to copy the output into the output
            continue
        with open(filename, 'rb') as readfile:
            shutil.copyfileobj(readfile, outfile)
