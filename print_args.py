import sys
import hashlib


def hash_string(str_to_hash,num_characters=20):
    '''
    hash function for each string
    '''
    return hashlib.sha1(str_to_hash).hexdigest()[:num_characters]


print sys.argv[1:],hash_string(sys.argv[1],4)
