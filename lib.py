import pandas as pd
import numpy as np
from haversine import haversine
import sys
from cy_lib import *
import time
import itertools

import matplotlib.pyplot as plt

import warnings
warnings.simplefilter(action = "ignore")

north_pole = (90,0)
weight_limit = 1000
sleigh_weight = 10
gifts = pd.read_csv('gifts.csv')

def window(seq, n=2):
    "Returns a sliding window (of width n) over data from the iterable"
    "   s -> (s0,s1,...s[n-1]), (s1,s2,...,sn), ...                   "
    it = iter(seq)
    result = tuple(itertools.islice(it, n))
    if len(result) == n:
        yield result
    for elem in it:
        result = result[1:] + (elem,)
        yield result

def score_trip(this_trip):
    return weighted_trip_length(
            this_trip[['Latitude','Longitude']],
            this_trip.Weight.tolist())


def weighted_trip_length(stops, weights,sleigh_weight=10):
    tuples = [tuple(x) for x in stops.values]
    # adding the last trip back to north pole, with just the sleigh weight
    tuples.append(north_pole)
    weights.append(sleigh_weight)

    dist = 0.0
    prev_stop = north_pole
    prev_weight = sum(weights)
    for location, weight in zip(tuples, weights):
        dist = dist + haversine(location, prev_stop) * prev_weight
        prev_stop = location
        prev_weight = prev_weight - weight
    return dist

def weighted_reindeer_weariness(all_trips):
    uniq_trips = all_trips.TripId.unique()

    if any(all_trips.groupby('TripId').Weight.sum() > weight_limit):
        raise Exception("One of the sleighs over weight limit!")

    dist = 0.0
    for t in uniq_trips:
        this_trip = all_trips[all_trips.TripId==t]
        dist += weighted_trip_length(
            this_trip[['Latitude','Longitude']],
            this_trip.Weight.tolist())

    return dist

def score_submission(sample_sub):
    all_trips = sample_sub.merge(gifts, on='GiftId')
    return weighted_reindeer_weariness(all_trips)



NP = np.array([[0,0,.5]])
SW = 10
AVG_EARTH_RADIUS = 6371
def add_cords(cords):
    cords_rad = cords*np.pi/180

    x = 0.5 * np.cos(cords_rad[:,0]) * np.sin(cords_rad[:,1])
    y = 0.5 * np.cos(cords_rad[:,0]) * np.cos(cords_rad[:,1])
    z = 0.5 * np.sin(cords_rad[:,0])
    return np.vstack([x,y,z]).T

# quick haversine
def quick_wrw(pts,w=None,nan_penalty=None):

    penalty_flag=0
    if w is None:
        w = pts[:,3]
        pts = pts[:,:3]

    if sum(w)>1000:
        if nan_penalty is None:
            return np.nan
        penalty_flag = 1


    # add north pole
    pts_poles = np.vstack([NP,pts,NP])

    # split list in two
    diff_mat = pts_poles[:-1]-pts_poles[1:]

    # calc distances
    diff_dist = np.linalg.norm(diff_mat,2,axis=1)
    diff_haversine = 2 * AVG_EARTH_RADIUS * np.arcsin(diff_dist)


    # reverse cumsum weights
    cs = w[::-1].cumsum()[::-1]+SW
    cs = np.hstack([cs,[SW]])

    # compute wrw
    score = np.sum(diff_haversine*cs)/1e9

    if nan_penalty is None:
        return score
    else:
        return score+nan_penalty*penalty_flag*sum(w)

def quick_wrw_pandas(df):
    return quick_wrw(df[["x","y","z","Weight"]].values)

def sort_and_quick_wrw(gifts):
    if gifts.Weight.sum()>weight_limit:
        return np.nan
    a = gifts.sort_values("Latitude",ascending=0)
    a["TripId"] = 1
    return quick_total_score(a)

# quick haversine
def raw_quick_wrw(pts,w):

    # split list in two
    diff_mat = pts[:-1]-pts[1:]

    # calc distances
    diff_dist = np.linalg.norm(diff_mat,2,axis=1)
    diff_haversine = 2 * AVG_EARTH_RADIUS * np.arcsin(diff_dist)


    # reverse cumsum weights
    cs = w[1:]+SW

    # compute wrw
    return np.sum(diff_haversine*cs)/1e9


def sort_and_quick_ts(gifts):
    return quick_total_score(gifts.sort_values(["TripId","Latitude"],
        ascending=[1,0]))

def quick_total_score(gifts_xyz_opt):
    # get trips in order
    gifts_xyz_opt = gifts_xyz_opt \
            .set_index(["TripId","GiftId"]) \
            .sortlevel("TripId",sort_remaining=False) \
            .reset_index()

    gv = gifts_xyz_opt[["TripId","x","y","z","Weight"]].values
    if len(gifts_xyz_opt)==0:
        return np.nan
    st = int(gifts_xyz_opt["TripId"].min())
    en = int(gifts_xyz_opt["TripId"].max())
    N = en-st+1
    pp = np.hstack([np.arange(st,(en+2))[:,np.newaxis],
                    np.zeros([N+1,2]),
                    np.ones([N+1,1])*.5,
                    np.zeros([N+1,1])])

    inds=np.searchsorted(gv[:,0],pp[:,0])
    gifts_xyz_opt_w_poles_np = np.insert(gv,inds,pp,axis=0)

    return raw_quick_wrw(gifts_xyz_opt_w_poles_np[:,1:4],
                  calc_weights(gifts_xyz_opt_w_poles_np[::-1,4])[::-1])


def optimize_and_score(gifts_xyz,max_weight=1000):
    gifts_xyz = gifts_xyz.sort_values("Longitude")
    gifts_xyz["TripId"] = optimize(gifts_xyz.Weight.values,max_weight)
    gifts_xyz_opt = gifts_xyz.sort_values(["TripId","Latitude"],ascending=[1,0])
    return quick_total_score(gifts_xyz_opt)



def trip_vals(keep,sa):
    '''
    convert keep matrix to tripids
    '''
    keep_np = np.array(keep)
    w = keep_np[np.argmin(keep_np[:,0]),5]
    st = int(keep_np[np.argmin(keep_np[:,0]),6])

    opt = optimize(sa[st:].Weight.values,w)

    lens = keep_np[:(np.argmin(keep_np[:,0])+1),1:4].flatten()
    opt_lens = np.diff([-1]+list(np.where(np.diff(opt)>0)[0])+[len(opt)-1])
    all_len = np.hstack([lens,opt_lens])
    all_len
    return np.hstack([np.ones(i)*k for k,i in enumerate(all_len)])


def slow_opt(sec,perm,piece,opt_w):
    sa = pd.read_csv('sc/sc_{}_{}_{}_.csv'.format(sec,perm,piece))

    sa = sa.sort_values("Longitude")
    aaa = sa
    aaa["TripId"] = optimize(aaa.Weight.values,opt_w)+1
    aaa_opt = aaa.sort_values(["TripId","Latitude"],ascending=[1,0])
    min_len_1 = len(aaa_opt[aaa_opt.TripId==1])
    min_len_2 = len(aaa_opt[aaa_opt.TripId==2])
    min_len_tot = len(aaa_opt[aaa_opt.TripId.isin([1,2,3])])

    cur_ind = 0
    old_score = 0

    st = time.time()
    w = 8
    sp = 2
    keep = []
    while True:
    # for bb in [1]:
        wide_range = np.linspace(opt_w-150,min(1000,opt_w+150),11)
        narow_range = np.linspace(opt_w-15,min(1000,opt_w+15),6)

        rn1 = np.hstack([wide_range,narow_range])
        r1 = range(min_len_1-w,min_len_1+w+1,sp)
        r2 = range(min_len_2-w,min_len_2+w+1,sp)
        rtot = range(min_len_tot-w,min_len_tot+w+1,sp)
        out = np.empty([len(r1),len(r2),len(rtot),len(rn1)])
        len_1 = np.empty([len(rtot),len(rn1)],dtype=int)
        len_2 = np.empty([len(rtot),len(rn1)],dtype=int)
        len_tot = np.empty([len(rtot),len(rn1)],dtype=int)

        for i_n_tot,ntot in enumerate(rtot):
            aaa = sa[(ntot+cur_ind):]
            for i_k,k in enumerate(rn1):
                aaa["TripId"] = optimize(aaa.Weight.values,k)+1
                aaa_opt = aaa.sort_values(["TripId","Latitude"],ascending=[1,0])
                len_1[i_n_tot,i_k] = len(aaa_opt[aaa_opt.TripId==1])
                len_2[i_n_tot,i_k] = len(aaa_opt[aaa_opt.TripId==2])
                len_tot[i_n_tot,i_k] = len(aaa_opt[aaa_opt.TripId.isin([1,2,3])])
                if len(aaa_opt)==0:
                    out = np.nan
                    break
                t_n1 = quick_total_score(aaa_opt)

                for i_n,n in enumerate(r1):
                    t1 = sort_and_quick_wrw(sa[cur_ind:(n+cur_ind)])
                    for i_n_2,n2 in enumerate(r2):
                        if (n+cur_ind)>len(sa) or (n+n2+cur_ind)>len(sa):
                            out[i_n,i_n_2,i_n_tot,i_k] = np.nan
                            continue
                        t2 = sort_and_quick_wrw(sa[(n+cur_ind):(n+n2+cur_ind)])
                        t3 = sort_and_quick_wrw(sa[(n+n2+cur_ind):(ntot+cur_ind)])
                        out[i_n,i_n_2,i_n_tot,i_k] = (old_score
                                                        + t1
                                                        + t2
                                                        + t3
                                                        + t_n1)
    #     print out
        if np.all(np.isnan(out)):
            break

        min_ind = np.nanargmin(out)

        r1_ind,r2_ind,rtot_ind,rn1_ind = np.unravel_index(min_ind,[len(r1),len(r2),len(rtot),len(rn1)])

        min_val = out.flatten()[min_ind]
        min_len_1 = len_1[rtot_ind,rn1_ind]
        min_len_2 = len_2[rtot_ind,rn1_ind]
        min_len_tot = len_tot[rtot_ind,rn1_ind]


        old_score += (sort_and_quick_wrw(sa[cur_ind:(r1[r1_ind]+cur_ind)])
                + sort_and_quick_wrw(sa[(r1[r1_ind]+cur_ind):(r1[r1_ind]+r2[r2_ind]+cur_ind)])
                + sort_and_quick_wrw(sa[(r1[r1_ind]+r2[r2_ind]+cur_ind):(rtot[rtot_ind]+cur_ind)]))
        cur_ind += rtot[rtot_ind]

        keep += [[min_val,r1[r1_ind],r2[r2_ind],rtot[rtot_ind]-r1[r1_ind]-r2[r2_ind],rtot[rtot_ind],rn1[rn1_ind],
               cur_ind,
               old_score,(time.time()-st)/60]]

        sa["TripId"] = trip_vals(keep,sa)
        sa.to_csv("keeper/k_{}_{}_{}_.csv".format(sec,perm,piece),index=None)

        opt_w = rn1[rn1_ind]

        print sec,perm,piece,keep[-1]

def two_way_score(t0,t1):
    return quick_wrw(t0[:,1:],nan_penalty=100)+quick_wrw(t1[:,1:],nan_penalty=100)

def one_way_score(t0):
    return quick_wrw(t0[:,1:],nan_penalty=100)

def check_pair_improvement(t0_temp,t1_temp,t0,t1,new_standard):
    score = two_way_score(t0_temp,t1_temp)
    if score<new_standard:
        t0,t1 = np.copy(t0_temp),np.copy(t1_temp)
        new_standard = score
    return t0,t1,new_standard


def check_single_improvement(t0_temp,t0,new_standard):
    score = one_way_score(t0_temp)
    change = False
    if score<new_standard:
        t0 = np.copy(t0_temp)
        new_standard = score
        change = True
    return t0,new_standard,change

def bb_sort(ll):
    standard = one_way_score(ll)
    lcopy = np.copy(ll)
    s_limit = 5000000
    optimal = False
    while not optimal:
        optimal = True
        for i in range(len(ll)-1):
            np.copyto(lcopy,ll)

            # flip samples
            lcopy[i,:], lcopy[i+1,:] = ll[i+1,:], ll[i,:]

            # check improvement
            ll,standard,change = check_single_improvement(lcopy,ll,standard)
            if change:
                # print "sort worked", standard
                optimal = not change
                s_limit -= 1
                if s_limit < 0:
                    break
    return ll

def import_submission(f_string):
    df2 = pd.read_csv(f_string)
    gifts = pd.read_csv("gifts.csv")
    cords = gifts[["Latitude","Longitude"]].values
    gifts[["x","y","z"]] = pd.DataFrame(add_cords(cords))
    df2 = df2.merge(gifts, on="GiftId")
    df_score_2 = df2.groupby("TripId").apply(quick_wrw_pandas).reset_index()
    df_score_2.columns = ["TripId","score"]
    df2 = df2.merge(df_score_2, on="TripId")
    return df2



def combo_optimizer(df2,trips):
    st = time.time()
    df2 = df2[["GiftId","x","y","z","Weight","TripId"]]
    df2 = sort_trips(df2,trips)


    # extract only trips of interest
    df2 = df2[df2.TripId.isin(trips)]
    print quick_total_score(df2),"===================================="


    for k0_,k1_ in sorted(list(itertools.combinations(trips,2)),key = lambda k: abs(k[1]-k[0])):
        t0 = df2[df2.TripId.isin([k0_])][["GiftId","x","y","z","Weight"]].values
        t1 = df2[df2.TripId.isin([k1_])][["GiftId","x","y","z","Weight"]].values
        standard = two_way_score(t0,t1)
        new_standard = two_way_score(t0,t1)
        print k0_,k1_,standard,"++++++++++++++",round((time.time()-st)/60,2)

        # track change
        changed = False
        while True:
            for r0_,r1_ in sorted(list(itertools.product(range(len(t0)),range(len(t1)))),key = lambda k: abs(k[1]-k[0])):

                #### hack; could probably be better
                if r0_>=len(t0)or r1_>=len(t1):
                    continue

                # flip samples
                t0_temp = np.vstack([t0[:r0_],
                                     t1[r1_,:],
                                     t0[(r0_+1):]])
                t1_temp = np.vstack([t1[:r1_],
                                     t0[r0_,:],
                                     t1[(r1_+1):]])
                t0,t1,new_standard = check_pair_improvement(t0_temp,t1_temp,t0,t1,new_standard)

                # try moving t1[r1_] to t0[r0_]
                t0_temp = np.vstack([t0[:r0_],
                                     t1[r1_,:],
                                     t0[r0_:]])
                t1_temp = np.vstack([t1[:r1_],
                                     t1[(r1_+1):]])
                t0,t1,new_standard = check_pair_improvement(t0_temp,t1_temp,t0,t1,new_standard)


                # try moving t0[r0_] to t1[r1_]
                t0_temp = np.vstack([t0[:r0_],
                                     t0[(r0_+1):]])
                t1_temp = np.vstack([t1[:r1_],
                                     t0[r0_,:],
                                     t1[r1_:]])
                t0,t1,new_standard = check_pair_improvement(t0_temp,t1_temp,t0,t1,new_standard)

            # optimize ordering
            t0 = bb_sort(t0)
            t1 = bb_sort(t1)



            # track a change
            if standard!=new_standard:
                print standard, new_standard, standard-new_standard
                changed = True
            if standard-new_standard<1e-8:
                break
            standard = new_standard



        # solidify new trip ids for next iteration
        if changed:
            df2 = df2[~df2.TripId.isin([k0_,k1_])]
            new_trips = pd.DataFrame(np.vstack([t0,t1]),columns=["GiftId","x","y","z","Weight"])
            new_trips["TripId"] = np.hstack([[k0_]*len(t0),[k1_]*len(t1)]).astype(int)
            df2 = pd.concat([df2,new_trips])
            print "wrote change to df", quick_total_score(df2)

    return df2,quick_total_score(df2)


def lon(t0):
    xx,yy = list(np.mean(t0[:,1:3],axis=0))
    return np.arctan(xx/yy)*360/2/np.pi


def get_changes(storage):
    a = [[k[0],k[1],v[0]] for k,v in storage.items()]
    a = pd.DataFrame(a,columns=["t0","t1","score_diff"])
    a = a.sort_values("score_diff", ascending=0)
    used = []
    keepers = []
    for row in a.to_records():
        if (row.t0 in used) or (row.t1 in used):
            continue
        keepers.append(list(row)[1:3])
        used.append(row.t0)
        used.append(row.t1)
    return keepers

def update_changes(storage,df2):
    keepers = get_changes(storage)
    print len(keepers)," changes"
    print keepers
    for keep_row in keepers:
        k0_ = keep_row[0]
        k1_ = keep_row[1]
        t0 = storage[tuple(keep_row)][1]
        t1 = storage[tuple(keep_row)][2]
        df2 = df2[~df2.TripId.isin([k0_,k1_])]
        new_trips = pd.DataFrame(np.vstack([t0,t1]),columns=["GiftId","x","y","z","Weight"])
        new_trips["TripId"] = np.hstack([[k0_]*len(t0),[k1_]*len(t1)]).astype(int)
        df2 = pd.concat([df2,new_trips])
        print "wrote change to df", quick_total_score(df2)
    return df2

def sort_trips(df2,trips=None):

    if trips is None:
        trips = np.unique(df2.TripId)

    for tr in trips:
        t0 = df2[df2.TripId.isin([tr])][["GiftId","x","y","z","Weight"]].sort_values("z",ascending=0).values
        t0 = bb_sort(t0)
        df2 = df2[~df2.TripId.isin([tr])]
        new_trips = pd.DataFrame(t0,columns=["GiftId","x","y","z","Weight"])
        new_trips["TripId"] = np.array([tr]*len(t0)).astype(int)
        df2 = pd.concat([df2[["GiftId","x","y","z","Weight","TripId"]],new_trips])
#         print "wrote change to df", quick_total_score(df2)
    return df2

def plot_trips(df,trips):
    col = ['r','k','b','g','c','m','y']

    df = df[["GiftId","Weight","TripId"]].merge(gifts[["GiftId","Latitude","Longitude"]],on="GiftId")

    k = 0
    for l,dd in df[df.TripId.isin(trips)].groupby("TripId"):
        k+=1
        c = col[k%len(col)]
        ww = str(int(np.sum(dd.Weight)))
        tt = str(int(np.mean(dd.TripId)))
        plt.plot(dd.Longitude,dd.Latitude,c+'-o')
        plt.text(dd.Longitude.values[0],dd.Latitude.values[0]+10+np.random.rand()*15,ww+","+tt)
    return df

if __name__ == "__main__":

    sub = sys.argv[1]
    sample_sub = import_submission(sub)
    print quick_total_score(sample_sub)
