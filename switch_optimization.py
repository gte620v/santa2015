import sys
from lib import *
import time
import hashlib


def hash_string(str_to_hash,num_characters=20):
    '''
    hash function for each string
    '''
    return hashlib.sha1(str_to_hash).hexdigest()[:num_characters]



trips = map(int,sys.argv[1].split(","))


file_in_name = sys.argv[2]

file_out_prefix = sys.argv[3]
file_index = hash_string(sys.argv[1],8)


df2 = import_submission(file_in_name)

# do optimization
df2,_ = combo_optimizer(df2,trips)

# save file
df2.to_csv(file_out_prefix+file_index,index=None)
print "wrote to ",file_out_prefix+file_index





