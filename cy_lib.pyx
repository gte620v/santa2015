cimport numpy as np
def calc_weights(np.ndarray[np.float64_t] w):
    cdef int i
    cdef float temp=0
    cdef np.ndarray[np.float64_t] out_w=w*0
    for i in range(w.shape[0]):
        if w[i]>0.1:
            out_w[i] = temp+w[i]
        else:
            out_w[i] = w[i]
        temp = out_w[i]
    return out_w

def optimize(np.ndarray[np.float64_t] w,
             float max_weight):
    cdef int i
    cdef float cur_weight=0
    cdef int trip_id=0
    cdef np.ndarray[np.float64_t] out_w=w*0
    for i in range(w.shape[0]):
        if (cur_weight+w[i])>max_weight:
            trip_id += 1
            cur_weight = 0
        out_w[i] = trip_id
        cur_weight += w[i]
    return out_w
